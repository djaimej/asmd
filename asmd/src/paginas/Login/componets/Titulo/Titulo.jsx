import React from 'react';
import './Titulo.css';

const Titulo = ({text}) =>{
    return(
        <div className='titulo-Container'>
            <h2 className='titulo'>{text}</h2>
        </div>
    );
};
export default Titulo;