import React from 'react';
import './Input.css'

/*
    attributo maneja el contenido de id, type, name y placeholder
*/
const Input = ({attribute, handleChange, param}) =>{
    return(
        <div className='contenedor'>
            <input 
                id = {attribute.id}
                name = {attribute.name}
                type ={attribute.type}
                placeholder ={attribute.placeholder}
                onChange ={(e) => handleChange(e.target.name,e.target.value)}
                className = 'input-container'
            />
        </div>
    );
};
export default Input;