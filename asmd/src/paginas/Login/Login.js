import React, {useState} from 'react';
import './Login.css';
import './componets/Titulo/Titulo'
import Titulo from './componets/Titulo/Titulo';
import Input from './componets/Input/Input';
import { setUseProxies } from 'immer';



const Login = () =>{

    const [user,setUser] = useState('');
    const [password,setPassword] = useState('');
    function handleChange(name,value){
        if(name === 'usuario')
            setUser(value);
        else
            setPassword(value);
    }
    
    return(
        <div className='login-container'> 
            <div className='login-triangulo'></div>
            <Titulo text = 'iniciar'/>
            <form className='contenedor'>
                <p> <Input 
                    attribute={{
                        id: 'usuario',
                        name: 'usuario',
                        type: 'text',
                        placeholder: 'Usuario'
                    }}
                    handleChange={handleChange}
                /></p>
                <p><Input 
                    attribute={{
                        id: 'contraseña',
                        name: 'contraseña',
                        type: 'password',
                        placeholder: 'Contraseña'
                    }}
                    handleChange={handleChange}
                /></p>
                <p><input type="submit" value="Ingresar" href='/src/paginas/Menu/Menu.js'/></p>
            </form>
        </div>
    );
};

export default Login;