import React from 'react';
import {Container, Wrapper, Menu2, MenuItem,MenuItemLink} from './Botones-element';
import {FaHome,FaUserAlt} from 'react-icons/fa';
import { BsInfoCircleFill } from "react-icons/bs";
import { IconContext} from 'react-icons/lib';
import './Menu.css';
import Reproducir from './componets/viedeo/Reproductor';


const Menu = () =>{
    return(
        <div>
            <main>
                <Reproducir/>
            </main>
            <Container>
                <Wrapper>
                <IconContext.Provider value={{ style: { fontSize: "2.5em" } }}>
                    <Menu2>
                        <MenuItem>
                            <MenuItemLink >
                                <FaHome />
                            </MenuItemLink>
                        </MenuItem>
                        <MenuItem>
                            <MenuItemLink >
                                <BsInfoCircleFill/>
                            </MenuItemLink>
                        </MenuItem>
                        <MenuItem>
                            <MenuItemLink >
                                <FaUserAlt/>
                            </MenuItemLink>
                        </MenuItem>
                    </Menu2>
                </IconContext.Provider>
                </Wrapper>
            </Container>
        </div>
    );
};
export default Menu;