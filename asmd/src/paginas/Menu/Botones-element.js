import styled from 'styled-components';


export const Container = styled.div`
    width: 100%;
    height: 100px;
    background-color:  #000000   ;
`;

export const Wrapper = styled.div`
    width: 100%;
    max-width: 1300px; 
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    margin: auto;
    height: 70px;
`;

export const LogoContainer = styled.div`
    margin-left: 0.5rem;
    display: flex;
    align-items: center;
    font-size: 1.2rem; 
    font-family: Agency FB, sans-serif;

`;

export const Menu2  = styled.ul`
    height: 70%;
    display:flex;
    justify-content: space-between;
    list-style:none;
`;

export const MenuItem = styled.li`
    height: 100%;
`;

export const MenuItemLink = styled.a`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    padding: 0.5rem 2.5rem;
    color: #64b2ff;
    font-family: sans-serif;
    font-size: 1rem;
    font-weight: 300;
    cursor: pointer;
    transition: 0.5s all ease;

    &:hover {
        color: #fff;
        background-color: #ff0808;
        transition: 0.5s all ease;
        div {
            svg {
                fill: #23394d;
            }
        }
    }
`;
export const mobilIcon = styled.div`
    display:none;

    @media screen and (max-widht: 950px){
        display: flex;
        align-items: center;
        cursor:pointer
        svg{
            fill: #ffffff;
            margin-right: 0.5rem;
        }
    }
`;
