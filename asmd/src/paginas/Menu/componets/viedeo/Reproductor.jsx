import React from 'react';
import ReactPlayer from 'react-player';
import './Reproductor.css';
//https://www.youtube.com/watch?v=kGae729RcPI

const Reproducir = () =>{
    return(
        <div className ='player-wrapper'>
            <ReactPlayer 
                className = 'reproductor'
                url ='./Planeta del Tesoro.mp4'
                width = "90%"
                height = "80%"
                controls
            />
        </div>
    );
};
export default Reproducir;